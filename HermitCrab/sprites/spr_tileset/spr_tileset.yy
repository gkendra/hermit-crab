{
    "id": "7c96b065-2c3b-45ae-b15b-78fc4b9f7f0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e89de552-8edb-4636-8fc1-b8d822649ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c96b065-2c3b-45ae-b15b-78fc4b9f7f0b",
            "compositeImage": {
                "id": "eff65fc2-159f-4b0a-bdfc-964dc94c575d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89de552-8edb-4636-8fc1-b8d822649ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90298bc1-872c-45c3-8751-d670df13476b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89de552-8edb-4636-8fc1-b8d822649ec2",
                    "LayerId": "90c04c9f-0aa8-41ca-b410-0d31cc53ceb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "90c04c9f-0aa8-41ca-b410-0d31cc53ceb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c96b065-2c3b-45ae-b15b-78fc4b9f7f0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}