{
    "id": "20e6ee6b-dc15-4926-973d-3e49e940c00c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Arrows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de80e0ca-0ab2-4fd6-b432-09c7611219a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e6ee6b-dc15-4926-973d-3e49e940c00c",
            "compositeImage": {
                "id": "4486a63f-f3ae-41ec-a1ff-c8004d1f3f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de80e0ca-0ab2-4fd6-b432-09c7611219a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b7ff36e-d54a-42c3-a59e-3820c4f68e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de80e0ca-0ab2-4fd6-b432-09c7611219a8",
                    "LayerId": "23591927-a75d-40b1-9b3f-060c439b6b0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "23591927-a75d-40b1-9b3f-060c439b6b0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20e6ee6b-dc15-4926-973d-3e49e940c00c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}