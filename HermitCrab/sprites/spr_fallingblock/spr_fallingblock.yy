{
    "id": "df7cb2e0-2b0f-412c-bece-91d38f0c07ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fallingblock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28f20580-fb3d-4de3-9af1-f41b3de8f22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df7cb2e0-2b0f-412c-bece-91d38f0c07ae",
            "compositeImage": {
                "id": "49a4a0fb-3bec-4dc5-824e-195c2b5382f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f20580-fb3d-4de3-9af1-f41b3de8f22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "695acd77-fe0e-4aa1-bb0b-8a8252dba08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f20580-fb3d-4de3-9af1-f41b3de8f22d",
                    "LayerId": "8abdb08a-e3a7-4f2a-9ce5-b8ae789b16ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8abdb08a-e3a7-4f2a-9ce5-b8ae789b16ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df7cb2e0-2b0f-412c-bece-91d38f0c07ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}