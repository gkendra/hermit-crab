{
    "id": "e5edde71-4757-4eb1-99c7-6d54ae9dd455",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a71d1a63-ca5f-45f1-977f-2da2b5192edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5edde71-4757-4eb1-99c7-6d54ae9dd455",
            "compositeImage": {
                "id": "4131b539-f681-46df-a5c2-9e49dcfdf178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71d1a63-ca5f-45f1-977f-2da2b5192edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d562ca-ff0a-4de3-904e-c9810ddbdb30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71d1a63-ca5f-45f1-977f-2da2b5192edf",
                    "LayerId": "863a6910-1237-48bf-8f7a-a09721ae7932"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "863a6910-1237-48bf-8f7a-a09721ae7932",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5edde71-4757-4eb1-99c7-6d54ae9dd455",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}