{
    "id": "7f87efd0-6d06-49f9-abf3-86eb11fb2d34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spikes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fba2be1-4bdc-4b0b-af3c-f3c51d65be31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f87efd0-6d06-49f9-abf3-86eb11fb2d34",
            "compositeImage": {
                "id": "73690539-9525-4bc4-8c8b-4d7757dc2c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fba2be1-4bdc-4b0b-af3c-f3c51d65be31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f554b1fc-7f31-4416-8c69-b96416f5c859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fba2be1-4bdc-4b0b-af3c-f3c51d65be31",
                    "LayerId": "8899e800-3d2a-4bbc-bf1a-d2989b3b35f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8899e800-3d2a-4bbc-bf1a-d2989b3b35f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f87efd0-6d06-49f9-abf3-86eb11fb2d34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}