{
    "id": "f7c736c0-530c-4dba-9055-64af4dc19db8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "512ec429-d493-4362-b4ea-41733e003c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7c736c0-530c-4dba-9055-64af4dc19db8",
            "compositeImage": {
                "id": "2a2ff93f-0369-4b80-9b7d-deca41ee0f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512ec429-d493-4362-b4ea-41733e003c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceee3d14-c752-4b18-9631-71608a9a39ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512ec429-d493-4362-b4ea-41733e003c73",
                    "LayerId": "b3828ef0-88f1-4575-bb32-2bc1c9b63295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b3828ef0-88f1-4575-bb32-2bc1c9b63295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7c736c0-530c-4dba-9055-64af4dc19db8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}