{
    "id": "91885eba-edae-4669-ba07-1cfa23cc51b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sadFishSad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 35,
    "bbox_right": 92,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d194793-6fbe-4b70-a329-fb31c1f9cc92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91885eba-edae-4669-ba07-1cfa23cc51b9",
            "compositeImage": {
                "id": "e90f5bfd-037a-440d-9dc3-89f236ee932f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d194793-6fbe-4b70-a329-fb31c1f9cc92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59f0c0b-6d06-4256-961d-57ba1e956899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d194793-6fbe-4b70-a329-fb31c1f9cc92",
                    "LayerId": "9308e16a-1f88-4e82-9dc8-4a2e41a204fd"
                }
            ]
        },
        {
            "id": "022ec001-5f56-4d91-b39b-be266fcd747b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91885eba-edae-4669-ba07-1cfa23cc51b9",
            "compositeImage": {
                "id": "5915afef-9ed4-4c64-8461-5382e53fd803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "022ec001-5f56-4d91-b39b-be266fcd747b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27776828-50fd-415e-b8cf-58619bdb11f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "022ec001-5f56-4d91-b39b-be266fcd747b",
                    "LayerId": "9308e16a-1f88-4e82-9dc8-4a2e41a204fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9308e16a-1f88-4e82-9dc8-4a2e41a204fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91885eba-edae-4669-ba07-1cfa23cc51b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 31
}