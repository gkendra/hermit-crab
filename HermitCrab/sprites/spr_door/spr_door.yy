{
    "id": "1879eae1-7bb8-404c-a805-86798b1ad191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9e39ded-8096-401f-a900-6ebf27c0f62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1879eae1-7bb8-404c-a805-86798b1ad191",
            "compositeImage": {
                "id": "4af0858e-6b46-47eb-b759-0eab61f3ac9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e39ded-8096-401f-a900-6ebf27c0f62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3652b726-effd-47aa-af1d-f3663b0a31fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e39ded-8096-401f-a900-6ebf27c0f62b",
                    "LayerId": "2bda910a-31f3-4aac-9e19-703c95e7fbc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2bda910a-31f3-4aac-9e19-703c95e7fbc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1879eae1-7bb8-404c-a805-86798b1ad191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}