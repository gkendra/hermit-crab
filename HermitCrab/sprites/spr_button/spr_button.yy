{
    "id": "5d0cac10-94cc-4269-9ba2-9da483a4b3fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce4883fc-482d-422b-9a61-5750008e3a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0cac10-94cc-4269-9ba2-9da483a4b3fa",
            "compositeImage": {
                "id": "98fe9952-7b52-4136-b11d-1d0888f9672d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4883fc-482d-422b-9a61-5750008e3a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ecee122-238b-4e0a-94b3-159c1601c32f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4883fc-482d-422b-9a61-5750008e3a40",
                    "LayerId": "ee410b25-2317-4c90-bf39-ecc41db8903b"
                }
            ]
        },
        {
            "id": "775f2eab-4421-4e3e-8261-175c845e83b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0cac10-94cc-4269-9ba2-9da483a4b3fa",
            "compositeImage": {
                "id": "4d392134-33fd-4cb4-b64b-cd90a72f04ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775f2eab-4421-4e3e-8261-175c845e83b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb7ff7fc-0708-48e8-a81c-6a23d8aecbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775f2eab-4421-4e3e-8261-175c845e83b9",
                    "LayerId": "ee410b25-2317-4c90-bf39-ecc41db8903b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ee410b25-2317-4c90-bf39-ecc41db8903b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d0cac10-94cc-4269-9ba2-9da483a4b3fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}