{
    "id": "1f9c6a0e-f056-455c-b3ba-0237d96f8c85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fallingblock2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2e7b06f-649e-4166-afed-314fa4e43257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f9c6a0e-f056-455c-b3ba-0237d96f8c85",
            "compositeImage": {
                "id": "7d0a7eea-e2ec-4707-bc7a-f3860e18fdde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e7b06f-649e-4166-afed-314fa4e43257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42bd621-b534-4cd4-9a12-8445bfa3ff2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e7b06f-649e-4166-afed-314fa4e43257",
                    "LayerId": "9b652085-dba9-4bdd-b032-494c8a9d55d6"
                }
            ]
        },
        {
            "id": "265686d0-4605-47d2-b511-315962b49a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f9c6a0e-f056-455c-b3ba-0237d96f8c85",
            "compositeImage": {
                "id": "58845eba-f73f-4d0a-afb1-eba08dee9728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265686d0-4605-47d2-b511-315962b49a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6316cac5-f1d2-457b-a965-6c6d4e9adc38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265686d0-4605-47d2-b511-315962b49a61",
                    "LayerId": "9b652085-dba9-4bdd-b032-494c8a9d55d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b652085-dba9-4bdd-b032-494c8a9d55d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f9c6a0e-f056-455c-b3ba-0237d96f8c85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}