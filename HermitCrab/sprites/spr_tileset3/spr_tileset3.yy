{
    "id": "8ae71263-2e7d-4cf2-b110-1a29cee1a132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99ba118d-9476-4427-91db-b8fe066f2a4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae71263-2e7d-4cf2-b110-1a29cee1a132",
            "compositeImage": {
                "id": "aa098f07-384f-4b1b-aa3b-320ab8c8c964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ba118d-9476-4427-91db-b8fe066f2a4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d485b138-1b8f-4273-84ef-e5ebbdcd1934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ba118d-9476-4427-91db-b8fe066f2a4b",
                    "LayerId": "3e5c188a-b335-4911-b8f1-bbf2a65225d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3e5c188a-b335-4911-b8f1-bbf2a65225d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ae71263-2e7d-4cf2-b110-1a29cee1a132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}