{
    "id": "7932a891-ac30-4ab9-a785-daebac2828d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heavybutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c67e03c-5e19-4496-818a-d9ab136e21ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7932a891-ac30-4ab9-a785-daebac2828d5",
            "compositeImage": {
                "id": "33f08a9f-e763-4d08-b85f-0a575055b1c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c67e03c-5e19-4496-818a-d9ab136e21ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8618fedb-4a4f-4ce6-8f1a-1439c3ff5f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c67e03c-5e19-4496-818a-d9ab136e21ee",
                    "LayerId": "749c771f-06fc-41b2-ac28-cc5b010e612f"
                }
            ]
        },
        {
            "id": "52876a02-f62c-45d5-990f-370e2b856d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7932a891-ac30-4ab9-a785-daebac2828d5",
            "compositeImage": {
                "id": "12c3eaf4-d62c-4502-a227-bb38476d153f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52876a02-f62c-45d5-990f-370e2b856d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f9e513-64e5-430a-b925-2fb4a29bd4cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52876a02-f62c-45d5-990f-370e2b856d44",
                    "LayerId": "749c771f-06fc-41b2-ac28-cc5b010e612f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "749c771f-06fc-41b2-ac28-cc5b010e612f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7932a891-ac30-4ab9-a785-daebac2828d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}