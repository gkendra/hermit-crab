{
    "id": "d3ecc905-a134-4910-aed6-dcb026adfe1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WASD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ecf7a1d-8828-4d30-9c9a-c785815f64ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3ecc905-a134-4910-aed6-dcb026adfe1a",
            "compositeImage": {
                "id": "7a2030e9-538e-4922-bc17-c112671abfbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ecf7a1d-8828-4d30-9c9a-c785815f64ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59de435-9361-449a-af42-f50d13e1081c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecf7a1d-8828-4d30-9c9a-c785815f64ca",
                    "LayerId": "32468146-a2f5-4789-8483-75546d80fb6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "32468146-a2f5-4789-8483-75546d80fb6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3ecc905-a134-4910-aed6-dcb026adfe1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}