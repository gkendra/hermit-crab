{
    "id": "7287caeb-86e5-4b60-a449-98f146b6bd76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa79ee9-d768-43fe-ab2c-7ff5fc9975c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7287caeb-86e5-4b60-a449-98f146b6bd76",
            "compositeImage": {
                "id": "f66e0d4e-999c-440e-99a2-491d0249766b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa79ee9-d768-43fe-ab2c-7ff5fc9975c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5201767b-1042-47dd-884b-7c98f1bb3ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa79ee9-d768-43fe-ab2c-7ff5fc9975c3",
                    "LayerId": "fe8255ed-4b91-4ecf-9acd-6f17f5c24587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe8255ed-4b91-4ecf-9acd-6f17f5c24587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7287caeb-86e5-4b60-a449-98f146b6bd76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}