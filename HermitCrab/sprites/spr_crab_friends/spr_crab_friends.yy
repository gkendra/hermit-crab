{
    "id": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crab_friends",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed9fb2e8-5a6c-4130-bcb3-728cd4054811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "bc983650-1d89-4a2c-a0f5-d5ec0d55134d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9fb2e8-5a6c-4130-bcb3-728cd4054811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b6e7da5-1dae-4ac3-84a2-5ea5b246ea8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9fb2e8-5a6c-4130-bcb3-728cd4054811",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        },
        {
            "id": "19fd1e50-3968-4f9f-a75b-fbeef4387582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "888f8a97-acd6-468c-adbb-50d991215c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fd1e50-3968-4f9f-a75b-fbeef4387582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aeea43c-a338-4b61-a36b-105432ffc386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fd1e50-3968-4f9f-a75b-fbeef4387582",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        },
        {
            "id": "815e3a9e-89ad-4265-a617-d58c6a52c598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "4f4a6bb1-5163-4db6-a0e6-786b8d8ea652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815e3a9e-89ad-4265-a617-d58c6a52c598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e6093e-5daa-4c63-9887-39977aaa04fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815e3a9e-89ad-4265-a617-d58c6a52c598",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        },
        {
            "id": "cddba6f9-2912-4ac8-a680-d09bd8a3c8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "dc9ec9c3-4ebc-47e3-bebe-11a29666f55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cddba6f9-2912-4ac8-a680-d09bd8a3c8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b39fc823-6065-4a47-a492-169efb4d17c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cddba6f9-2912-4ac8-a680-d09bd8a3c8d4",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        },
        {
            "id": "bc8617fe-c2ec-4fad-ad0e-f4db7333d960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "00f53ccb-aae1-4bb5-ab13-89f50f07ef74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc8617fe-c2ec-4fad-ad0e-f4db7333d960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812fdadb-48d8-4514-98c2-769846ed0464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8617fe-c2ec-4fad-ad0e-f4db7333d960",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        },
        {
            "id": "1373e0a0-e32a-4e25-bfc9-a149524f47df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "compositeImage": {
                "id": "ecc1f1d9-03c9-463f-b7be-de9f27bff185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1373e0a0-e32a-4e25-bfc9-a149524f47df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16fa6295-e282-4e47-9c98-382f99b0c6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1373e0a0-e32a-4e25-bfc9-a149524f47df",
                    "LayerId": "10221ad5-9013-40f7-88c0-ad7209c485bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "10221ad5-9013-40f7-88c0-ad7209c485bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}