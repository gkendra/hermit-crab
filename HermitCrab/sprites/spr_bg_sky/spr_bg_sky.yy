{
    "id": "ba8a439f-b41d-44da-83b2-293478f33777",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_sky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18d387a6-7bba-4ffa-8448-d28fb26d00b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8a439f-b41d-44da-83b2-293478f33777",
            "compositeImage": {
                "id": "d5b2049e-09ea-4f8b-8358-791780aff1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d387a6-7bba-4ffa-8448-d28fb26d00b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e60c76b-ef5d-417e-a3e6-f94bac39d513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d387a6-7bba-4ffa-8448-d28fb26d00b2",
                    "LayerId": "3204acd4-2003-4f72-ad95-fd2bfabaf9df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "3204acd4-2003-4f72-ad95-fd2bfabaf9df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba8a439f-b41d-44da-83b2-293478f33777",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}