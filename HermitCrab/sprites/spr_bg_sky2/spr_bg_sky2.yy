{
    "id": "b98a6519-33a2-49f0-8b77-96e4205f338a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_sky2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef9505fe-3a02-4b42-b221-666f1ba391be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b98a6519-33a2-49f0-8b77-96e4205f338a",
            "compositeImage": {
                "id": "28ca5726-44aa-41cb-bb8d-b43618789f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef9505fe-3a02-4b42-b221-666f1ba391be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01855679-3777-4436-a0c4-de6ba73b741e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef9505fe-3a02-4b42-b221-666f1ba391be",
                    "LayerId": "400eecf8-e836-491d-88c2-1c6d756f6957"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "400eecf8-e836-491d-88c2-1c6d756f6957",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b98a6519-33a2-49f0-8b77-96e4205f338a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}