{
    "id": "9d052223-8b62-4e68-92d5-fe2e35a53507",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c6d7e39-b8d3-403a-ba79-40129b2efe47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d052223-8b62-4e68-92d5-fe2e35a53507",
            "compositeImage": {
                "id": "7487800b-dbea-4bcd-ac14-e7faa003a56f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6d7e39-b8d3-403a-ba79-40129b2efe47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bd43c3-a428-4813-aaa9-db780e1fb855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6d7e39-b8d3-403a-ba79-40129b2efe47",
                    "LayerId": "423fab51-b4b4-43d2-9df3-63040b2ec9e1"
                }
            ]
        },
        {
            "id": "c7e53348-0381-4da2-a677-6d3a6e06d7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d052223-8b62-4e68-92d5-fe2e35a53507",
            "compositeImage": {
                "id": "845ceb24-3cc0-4753-99f3-6fcb315dd0b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e53348-0381-4da2-a677-6d3a6e06d7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a23627-2d6c-4cbc-9165-e87ddf959055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e53348-0381-4da2-a677-6d3a6e06d7a7",
                    "LayerId": "423fab51-b4b4-43d2-9df3-63040b2ec9e1"
                }
            ]
        },
        {
            "id": "198616fe-e149-4d26-96bc-f4ae943a8906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d052223-8b62-4e68-92d5-fe2e35a53507",
            "compositeImage": {
                "id": "7c7faf9f-f578-452f-a03c-bc91ff9cd3bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198616fe-e149-4d26-96bc-f4ae943a8906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a90e01e-0d2c-4493-bf80-39f0da42239a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198616fe-e149-4d26-96bc-f4ae943a8906",
                    "LayerId": "423fab51-b4b4-43d2-9df3-63040b2ec9e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "423fab51-b4b4-43d2-9df3-63040b2ec9e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d052223-8b62-4e68-92d5-fe2e35a53507",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}