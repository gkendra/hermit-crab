{
    "id": "c54def35-c920-4704-be7b-74c060664816",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88e99800-2309-4423-892b-b449e393a2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c54def35-c920-4704-be7b-74c060664816",
            "compositeImage": {
                "id": "6e25559a-7974-43c3-981b-1b220e731c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e99800-2309-4423-892b-b449e393a2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b0110fa-1c3e-46a5-bbca-387cac78642e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e99800-2309-4423-892b-b449e393a2d8",
                    "LayerId": "b5797d14-971c-4400-b9ba-cba0883f6038"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5797d14-971c-4400-b9ba-cba0883f6038",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c54def35-c920-4704-be7b-74c060664816",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}