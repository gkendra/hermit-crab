{
    "id": "1c0e5975-e978-40aa-9af1-97891d1c0532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sadFishHappy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 35,
    "bbox_right": 92,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63569109-926f-4b06-9cbf-43cb11012b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c0e5975-e978-40aa-9af1-97891d1c0532",
            "compositeImage": {
                "id": "5619ce8a-c1d4-4541-953f-320e5e68c0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63569109-926f-4b06-9cbf-43cb11012b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5aa936-b368-498f-9a99-fae4b9c2ee3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63569109-926f-4b06-9cbf-43cb11012b38",
                    "LayerId": "88a4d3d6-aa2a-43b2-9f42-7989c2acc257"
                }
            ]
        },
        {
            "id": "9ab22e77-d944-47a5-b202-07b091123bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c0e5975-e978-40aa-9af1-97891d1c0532",
            "compositeImage": {
                "id": "82a14972-48f8-4064-9d9b-81664dc20f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab22e77-d944-47a5-b202-07b091123bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d6d395-dc51-450a-8033-e94ec7167f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab22e77-d944-47a5-b202-07b091123bc4",
                    "LayerId": "88a4d3d6-aa2a-43b2-9f42-7989c2acc257"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "88a4d3d6-aa2a-43b2-9f42-7989c2acc257",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c0e5975-e978-40aa-9af1-97891d1c0532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 31
}