{
    "id": "4e338c79-a1de-4650-b83c-efa3c92d8bbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_original",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62086399-e5f9-4bd8-a46c-e3bb9f88285b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e338c79-a1de-4650-b83c-efa3c92d8bbb",
            "compositeImage": {
                "id": "c10e2afb-f5d0-4679-a601-a444db88323c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62086399-e5f9-4bd8-a46c-e3bb9f88285b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabd04d3-39c4-4f5a-ad08-7f8fcf28b0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62086399-e5f9-4bd8-a46c-e3bb9f88285b",
                    "LayerId": "27252595-33e8-45d0-ac2b-c729894daa9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "27252595-33e8-45d0-ac2b-c729894daa9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e338c79-a1de-4650-b83c-efa3c92d8bbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}