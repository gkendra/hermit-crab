{
    "id": "4b7034fe-8f54-406f-91aa-34dd32ec803d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "692e1cab-b2fc-4bfe-99ca-16c85d8c2dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b7034fe-8f54-406f-91aa-34dd32ec803d",
            "compositeImage": {
                "id": "c30fb396-b366-46e2-bdcb-91ed435e0683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692e1cab-b2fc-4bfe-99ca-16c85d8c2dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e9294c0-da97-46ca-ab87-fde2e6f1146e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692e1cab-b2fc-4bfe-99ca-16c85d8c2dad",
                    "LayerId": "714c0608-52be-4f33-8632-93bcc7049ea1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "714c0608-52be-4f33-8632-93bcc7049ea1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b7034fe-8f54-406f-91aa-34dd32ec803d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}