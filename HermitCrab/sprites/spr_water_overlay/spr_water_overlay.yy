{
    "id": "9f026945-7ede-4d14-b325-b59be61abda7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_overlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca6bddb7-7cc1-4d0d-8119-50949452ef14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f026945-7ede-4d14-b325-b59be61abda7",
            "compositeImage": {
                "id": "ba0f5e81-81a8-411d-bc1b-d5852b2822d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6bddb7-7cc1-4d0d-8119-50949452ef14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6662ce16-8f78-41a1-9036-300a4711e6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6bddb7-7cc1-4d0d-8119-50949452ef14",
                    "LayerId": "af520ecf-bd9a-4890-8686-a416a155bf5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "af520ecf-bd9a-4890-8686-a416a155bf5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f026945-7ede-4d14-b325-b59be61abda7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}