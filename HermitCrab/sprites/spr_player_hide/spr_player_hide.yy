{
    "id": "2004c3f3-4138-4387-96fd-43c34b987d6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_hide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a570558f-943e-4473-ac87-9ce9e73e527b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2004c3f3-4138-4387-96fd-43c34b987d6d",
            "compositeImage": {
                "id": "ebf10567-2b02-4f54-9e51-e59bc46f6a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a570558f-943e-4473-ac87-9ce9e73e527b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28564ac-1d44-4f8e-98a4-f2e2ed4785a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a570558f-943e-4473-ac87-9ce9e73e527b",
                    "LayerId": "0c57bd88-014a-4040-8dc2-b785ea3229b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c57bd88-014a-4040-8dc2-b785ea3229b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2004c3f3-4138-4387-96fd-43c34b987d6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}