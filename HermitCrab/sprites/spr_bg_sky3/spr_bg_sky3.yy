{
    "id": "c4259c94-86ca-4693-a617-8e49bc4f70cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_sky3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b285bd0-dfb7-47e4-96b5-f57022efae62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4259c94-86ca-4693-a617-8e49bc4f70cc",
            "compositeImage": {
                "id": "83a49611-a944-4437-a9bf-96bfa3080ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b285bd0-dfb7-47e4-96b5-f57022efae62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bea0da-47b9-4994-afaf-5ea900a3f327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b285bd0-dfb7-47e4-96b5-f57022efae62",
                    "LayerId": "8f865b4a-350e-4fce-8791-b757115e6bef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "8f865b4a-350e-4fce-8791-b757115e6bef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4259c94-86ca-4693-a617-8e49bc4f70cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}