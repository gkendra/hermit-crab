{
    "id": "21eb21b0-f4aa-4dc1-b0a4-ecc4d158f9ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gateReverse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41ccf6e4-38b5-4d0c-8cfa-f323d1725464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21eb21b0-f4aa-4dc1-b0a4-ecc4d158f9ef",
            "compositeImage": {
                "id": "6f8adae4-3835-4af5-8b21-3e8255541454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ccf6e4-38b5-4d0c-8cfa-f323d1725464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a676aa-6964-404a-98d1-a8050c46cd11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ccf6e4-38b5-4d0c-8cfa-f323d1725464",
                    "LayerId": "29aa3139-e39d-4a6d-9e9a-4c7612c85fc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "29aa3139-e39d-4a6d-9e9a-4c7612c85fc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21eb21b0-f4aa-4dc1-b0a4-ecc4d158f9ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}