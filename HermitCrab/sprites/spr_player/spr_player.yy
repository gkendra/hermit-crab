{
    "id": "c4630b55-bbba-4b9a-8a7d-8cb0715b2b6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afdd1f0a-739b-4cf8-9438-d1c8a8907adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4630b55-bbba-4b9a-8a7d-8cb0715b2b6f",
            "compositeImage": {
                "id": "5d273498-b7fe-460c-8966-b01778d36fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afdd1f0a-739b-4cf8-9438-d1c8a8907adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63ac809-97e8-4e9b-94ff-7c54b5c58733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afdd1f0a-739b-4cf8-9438-d1c8a8907adb",
                    "LayerId": "d8e9cba1-98f7-4984-9e9f-db81898da6c2"
                }
            ]
        },
        {
            "id": "666d5f20-c38f-4592-8c88-e6c0c86eae95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4630b55-bbba-4b9a-8a7d-8cb0715b2b6f",
            "compositeImage": {
                "id": "a3421a29-2558-4ad2-93a7-a746eb260b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666d5f20-c38f-4592-8c88-e6c0c86eae95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67501450-5391-4f67-a675-f25d9e71ee05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666d5f20-c38f-4592-8c88-e6c0c86eae95",
                    "LayerId": "d8e9cba1-98f7-4984-9e9f-db81898da6c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8e9cba1-98f7-4984-9e9f-db81898da6c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4630b55-bbba-4b9a-8a7d-8cb0715b2b6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}