{
    "id": "a0f926b7-0349-4695-95c8-7d2965ec1971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_sky_original",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85b51cbd-193d-4fbb-9de7-437009ba5111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0f926b7-0349-4695-95c8-7d2965ec1971",
            "compositeImage": {
                "id": "0e0ef82d-c5ce-477e-b5d1-c1d5a8cd5350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b51cbd-193d-4fbb-9de7-437009ba5111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3548048-c0b9-4a1b-bbda-33c1cc5dc51f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b51cbd-193d-4fbb-9de7-437009ba5111",
                    "LayerId": "a18fffac-3c95-43ca-9af9-1c4e3b1936c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "a18fffac-3c95-43ca-9af9-1c4e3b1936c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0f926b7-0349-4695-95c8-7d2965ec1971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}