{
    "id": "86413b84-ca6a-4da8-b545-c62d2796b15c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_space",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46605661-7dab-4ec1-80dc-6521d92d3d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86413b84-ca6a-4da8-b545-c62d2796b15c",
            "compositeImage": {
                "id": "63c26344-35c1-4ab8-91a7-8df5a6bb11a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46605661-7dab-4ec1-80dc-6521d92d3d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a88fb239-0c06-415d-86f8-e5b591f4ad14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46605661-7dab-4ec1-80dc-6521d92d3d50",
                    "LayerId": "589deff8-fd14-481f-905d-1c57953311ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "589deff8-fd14-481f-905d-1c57953311ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86413b84-ca6a-4da8-b545-c62d2796b15c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}