{
    "id": "1e8ed9a7-14bf-4a94-9e99-6c05c843afab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_downarrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85d1367c-bfb2-4031-9455-71d6424e08a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8ed9a7-14bf-4a94-9e99-6c05c843afab",
            "compositeImage": {
                "id": "213453f8-2c4b-4fb4-850b-f03c31859d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d1367c-bfb2-4031-9455-71d6424e08a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d730e51-164e-433a-882c-158fa7638b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d1367c-bfb2-4031-9455-71d6424e08a4",
                    "LayerId": "51eea416-aef7-49c3-ac57-05c60e994212"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "51eea416-aef7-49c3-ac57-05c60e994212",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e8ed9a7-14bf-4a94-9e99-6c05c843afab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 11
}