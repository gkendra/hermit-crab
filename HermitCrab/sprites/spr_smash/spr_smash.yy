{
    "id": "d94e3a60-fdcb-4339-b189-290faa6c24d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "323f530d-a296-4473-8458-a078567cb50d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d94e3a60-fdcb-4339-b189-290faa6c24d8",
            "compositeImage": {
                "id": "5b301767-6e43-4920-9424-c70a4073b730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "323f530d-a296-4473-8458-a078567cb50d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50448e71-1efe-4924-9576-c1a8a9a069d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "323f530d-a296-4473-8458-a078567cb50d",
                    "LayerId": "c5922ccd-5225-4b3f-8265-fad03a8ce244"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c5922ccd-5225-4b3f-8265-fad03a8ce244",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d94e3a60-fdcb-4339-b189-290faa6c24d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": -5
}