{
    "id": "098247ee-cb92-4705-9ea8-6d5c0de07337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smashable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab2f82cf-336a-4407-8d99-5c235dd350c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "098247ee-cb92-4705-9ea8-6d5c0de07337",
            "compositeImage": {
                "id": "1bdd38e5-1e81-4009-b831-ab00827e7629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab2f82cf-336a-4407-8d99-5c235dd350c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8210a83d-a417-4122-91d0-ae86dd07724d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab2f82cf-336a-4407-8d99-5c235dd350c5",
                    "LayerId": "bf0441bd-db65-4142-acad-94a45793d3a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf0441bd-db65-4142-acad-94a45793d3a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "098247ee-cb92-4705-9ea8-6d5c0de07337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}