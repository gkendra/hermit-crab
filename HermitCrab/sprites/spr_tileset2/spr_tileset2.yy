{
    "id": "ddc09b9c-2037-4f58-a738-69824061e839",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0d73d04-60ef-4de5-9b4a-60fadc977b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddc09b9c-2037-4f58-a738-69824061e839",
            "compositeImage": {
                "id": "1c338193-651c-40cd-81ae-b1ce0858fa8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d73d04-60ef-4de5-9b4a-60fadc977b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1edd626d-da73-4df9-b979-7f1dd0b1eedf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d73d04-60ef-4de5-9b4a-60fadc977b9b",
                    "LayerId": "206626f0-162a-488e-b883-54be45f09d20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "206626f0-162a-488e-b883-54be45f09d20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddc09b9c-2037-4f58-a738-69824061e839",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}