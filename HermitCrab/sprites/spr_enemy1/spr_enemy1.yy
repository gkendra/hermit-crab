{
    "id": "66878562-0c8d-400a-9b25-b04ef9d32dfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52b655ef-f588-4799-a97d-bf4d12f11917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66878562-0c8d-400a-9b25-b04ef9d32dfd",
            "compositeImage": {
                "id": "dafb4a6e-a7ad-4ea8-91dc-74b5e197d6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b655ef-f588-4799-a97d-bf4d12f11917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b37757-b554-4242-a861-d791324de695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b655ef-f588-4799-a97d-bf4d12f11917",
                    "LayerId": "66e20eab-4abb-48b0-a09f-32084b01a0e1"
                }
            ]
        },
        {
            "id": "17bb9f07-417f-4ef5-81b3-8c8fa72d6f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66878562-0c8d-400a-9b25-b04ef9d32dfd",
            "compositeImage": {
                "id": "e32ab2ba-f413-4079-933f-74a2971e68be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17bb9f07-417f-4ef5-81b3-8c8fa72d6f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a96ca03-5ea6-4719-8b1f-3e5deae6d2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17bb9f07-417f-4ef5-81b3-8c8fa72d6f97",
                    "LayerId": "66e20eab-4abb-48b0-a09f-32084b01a0e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "66e20eab-4abb-48b0-a09f-32084b01a0e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66878562-0c8d-400a-9b25-b04ef9d32dfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}