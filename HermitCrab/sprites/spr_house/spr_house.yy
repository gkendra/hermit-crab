{
    "id": "572dadd0-b4a4-4bf6-9ba8-be776dc909fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5af6d3ab-bb96-48ee-b7fb-6581df74a563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "572dadd0-b4a4-4bf6-9ba8-be776dc909fe",
            "compositeImage": {
                "id": "7748faf7-b5b9-4d99-82ba-5e656e511a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af6d3ab-bb96-48ee-b7fb-6581df74a563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57dc57e3-6052-48a0-bda2-52daa4a548f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af6d3ab-bb96-48ee-b7fb-6581df74a563",
                    "LayerId": "57ef21d7-039e-49d9-a32a-e89eba4094bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "57ef21d7-039e-49d9-a32a-e89eba4094bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "572dadd0-b4a4-4bf6-9ba8-be776dc909fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 95
}