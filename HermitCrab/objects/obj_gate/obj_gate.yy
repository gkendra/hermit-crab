{
    "id": "508f320e-caff-4b35-85de-b159fe86e2ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gate",
    "eventList": [
        {
            "id": "8dab46d1-d6f8-4bff-aad7-a7dd0e8a5421",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "508f320e-caff-4b35-85de-b159fe86e2ba"
        },
        {
            "id": "3050ab0e-af92-48ec-8224-f81ff66da635",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "508f320e-caff-4b35-85de-b159fe86e2ba"
        },
        {
            "id": "8996918f-5759-4bc8-b19b-7e4a812099c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "508f320e-caff-4b35-85de-b159fe86e2ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "476be0bd-c201-442f-82cf-510cf06ca7f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7c736c0-530c-4dba-9055-64af4dc19db8",
    "visible": true
}