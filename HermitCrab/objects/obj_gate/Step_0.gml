vsp = 0
hsp = 0

if buttonIsPressed {
	if abs(x-endX)>moveSpeed {
		hsp = moveSpeed * sign(endX - x)
	}else{
		x = endX;
	}
	
	if abs(y-endY)>moveSpeed {
		vsp = moveSpeed * sign(endY - y)
	}else{
		y = endY;
	}
}

else if !buttonIsPressed {
	if abs(x-startX)>moveSpeed {
		hsp = moveSpeed * sign(startX - x)
	}else{
		x = startX;
	}
	
	if abs(y-startY)>moveSpeed {
		vsp = moveSpeed * sign(startY - y)
	}else{
		y = startY;
	}
} 


x += hsp 
y += vsp