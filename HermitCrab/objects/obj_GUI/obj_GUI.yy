{
    "id": "6341252d-6fcc-4e50-8482-09860a685c6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GUI",
    "eventList": [
        {
            "id": "407613e6-9857-4112-8664-2a922ceeaaae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6341252d-6fcc-4e50-8482-09860a685c6d"
        },
        {
            "id": "e89a4ca3-eff7-4dac-84f8-7549a29c52e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6341252d-6fcc-4e50-8482-09860a685c6d"
        },
        {
            "id": "54a8bfe1-b444-452f-a34a-28e094470246",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6341252d-6fcc-4e50-8482-09860a685c6d"
        },
        {
            "id": "34f07268-a0c5-48fe-ab1c-58ac805ca8a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6341252d-6fcc-4e50-8482-09860a685c6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}