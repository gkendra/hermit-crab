mywidth = display_get_width();
myheight = display_get_height();
scalefactor = 3;
gamewidth = mywidth/scalefactor;
gameheight = myheight/scalefactor;

view_enabled=true;
view_visible[0]=true;
view_scale = 2;
window_width = gamewidth * view_scale;
window_height = gameheight * view_scale;
view_wport[0] = window_width;
view_hport[0] = window_height;
surface_resize(application_surface, gamewidth, gameheight);
window_set_size(window_width, window_height);
window_set_position(display_get_width()/2-window_width/2, display_get_height()/2-window_height/2);
display_set_gui_size(gamewidth,gameheight);
//display_set_gui_maximize(2,2);

fade = 0;