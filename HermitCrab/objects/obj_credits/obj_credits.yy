{
    "id": "3aba80c7-1e96-4f65-b0d9-d7650ba7063c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credits",
    "eventList": [
        {
            "id": "e19d2545-d7ec-4023-8957-362465be4517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aba80c7-1e96-4f65-b0d9-d7650ba7063c"
        },
        {
            "id": "ea460518-1d84-4577-933b-f82607cec64f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3aba80c7-1e96-4f65-b0d9-d7650ba7063c"
        },
        {
            "id": "56fa4b4b-ac89-40e3-9b6d-a2a801d877fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3aba80c7-1e96-4f65-b0d9-d7650ba7063c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}