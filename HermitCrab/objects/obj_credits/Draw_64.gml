draw_set_color(c_white);
draw_set_font(fnt_text);
draw_set_alpha(fade);
draw_text(75,40,
"Art:\n   Vincent Abella\nMusic/Sound:\n   Vajaran Nall\nProgramming:\n   Tyler Garman\n   Gabe Kendra\n   Chen Wang\n\nUsing the font \"m6x11\" by Daniel Linssen");
draw_set_halign(fa_center);
draw_text(display_get_gui_width()/2,display_get_gui_height()-35,"Press escape to exit. Thanks for playing!");
draw_set_halign(fa_left);
draw_set_alpha(1);