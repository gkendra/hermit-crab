{
    "id": "0786f594-2c71-48b9-945f-2cf397ad1a13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bigFish",
    "eventList": [
        {
            "id": "974c16f6-115b-4f59-9472-97ea125f7364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0786f594-2c71-48b9-945f-2cf397ad1a13"
        },
        {
            "id": "4828fc7a-ede5-4a0b-9fe7-e42d3a76ce02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0786f594-2c71-48b9-945f-2cf397ad1a13"
        },
        {
            "id": "a8b18928-3d79-46f4-bc0c-4b2e9017ac2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0786f594-2c71-48b9-945f-2cf397ad1a13"
        },
        {
            "id": "9ea3f33d-083e-426b-91c6-9a0dc63c438d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0786f594-2c71-48b9-945f-2cf397ad1a13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c0e5975-e978-40aa-9af1-97891d1c0532",
    "visible": true
}