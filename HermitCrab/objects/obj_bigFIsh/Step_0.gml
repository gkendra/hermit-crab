
//if shell touches mouth, and its not owned by player make it jump a store it in myShell variable

if shellsRequired > 0 {
	var _shell = instance_place(x,y,obj_shell)

	if instance_exists(_shell) && _shell != obj_player.cShell {
		_shell.vsp -= shellBounce
		myShell = _shell

	}

	//if my shell variable exists, wait for shell to land from jump, and if player hasnt grabbed it, then eat it

	if myShell == obj_player.cShell {
		myShell = noone
		
	}
	
if instance_exists(myShell) {
		
	if myShell == obj_player.cShell {
		myShell = noone
		shellConsumeTimer = 0
	}
	
	shellConsumeTimer ++	
	
	if shellConsumeTimer >= shellConsumerTotalTime {
		instance_destroy(myShell)
		myShell = noone
		shellsRequired -= 1
		shellConsumeTimer = 0
		
		//animate
		anim_happy = true
		sprite_index = spr_sadFishHappy
		image_speed = anim_happySpeed
		
		if shellsRequired <= 0 {
			linkedObject.buttonIsPressed = true
			playSound(snd_unlock);
		}
	}
	
		
}

	//if instance_exists(myShell) && myShell.vsp = 0 {
	//	instance_destroy(myShell)
	//	myShell = noone
	//	shellsRequired -= 1
		
	//	//animate
	//	anim_happy = true
	//	sprite_index = spr_sadFishHappy
	//	image_speed = anim_happySpeed
		
	//	if shellsRequired <= 0 {
	//		linkedObject.buttonIsPressed = true
	//		playSound(snd_unlock);
	//	}
	//}

}


if anim_happy {
	anim_happyTimer ++
	
	if anim_happyTimer >= anim_happyTime {
	
	anim_happyTimer = 0
	anim_happy = false
	image_speed = anim_mopeSpeed
	sprite_index =  spr_sadFishSad
	}
}