 draw_self()
 
 if shellsRequired>0{
 draw_set_halign(fa_center);
 draw_set_valign(fa_middle);
 draw_set_font(fnt_text);
 draw_set_color(c_black);
 var vy = y-50 + sin(current_time/100)*2;
 draw_sprite(spr_textbubble,0,x,vy);
 draw_text(x,vy, string(shellsRequired) )
 draw_set_halign(fa_left);
 draw_set_valign(fa_top);
 }