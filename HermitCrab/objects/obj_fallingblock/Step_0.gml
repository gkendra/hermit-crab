
if !dead{
	if abs(obj_player.x - (x+16)) < 16{
		if active = false{
			active = true;
			alarm[0] = 30;
			sprite_index = spr_fallingblock2;
		}
	}

	if active{
		if alarm[0] = -1 {
			vsp=10;
			VerticalCollision();
			y+=vsp;
			if place_meeting(x, y+1, par_solid){
				dead = true;
				solid_ins = instance_create_layer(x, y, "Instances", obj_block);
				alarm[1] = room_speed*2;
				active = false;
				sprite_index = spr_fallingblock;
			}
		}
	}
}

if disappear{
	if image_alpha > 0{
		image_alpha-=0.1;
	}else{
		image_alpha = 0;
	}
}else{
	if image_alpha < 1{
		image_alpha+=0.1;
	}else{
		image_alpha = 1;
	}
}