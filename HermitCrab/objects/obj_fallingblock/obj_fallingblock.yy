{
    "id": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fallingblock",
    "eventList": [
        {
            "id": "7b56da41-76cd-40a6-a561-50fabb02cb50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "34faded9-0355-40b3-b26d-92e80e025a05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "fc439045-cd9a-4e04-9390-4ef543789fe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "508dbe2d-18b6-40be-927b-ba757a7d530b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d7a65c6d-c207-4b27-af2c-1dafb48481bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "a5e7c1cb-7432-4482-bb77-7f5cc4127bd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "a3cc61c7-d377-478c-bd24-01bac7dd3e1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        },
        {
            "id": "53fe3e58-b1c1-4c1a-abf1-bafc0d6450fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2b283e9a-1157-4257-8b9f-d8ee7f7789d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df7cb2e0-2b0f-412c-bece-91d38f0c07ae",
    "visible": true
}