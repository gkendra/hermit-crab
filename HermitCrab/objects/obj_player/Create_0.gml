moveSpeed = 4
aSpeed = 0.3 //Acceleration speed
cSpeed = 0;
moveDir = 1
onGround = false
facing = 1

frozen = false;
hsp = 0	
vsp = 0

khsp = 0; //knockback x speed


jumpVelocity = 12
grav = 1

moveBlock = noone; //Are you on a moving block?
mhsp = 0; //Moving block hsp

dead = false;

drawShellXOffset = 0
drawShellYOffset = 0

instance_create_layer(0,0,"Instances",Input);
instance_create_layer(0,0,"Instances",obj_GUI);
instance_create_layer(0,0,"Instances",obj_camera);
//Shells
//cShell = 0; //Which shell you currently have
//hasShell = true; //Whether or not the shell is on your back
pickupShell = noone;
shellSmash = false; //If you are currently using the down action for the hard shell
shellBounce = false; //If you are currently bouncing

paralyze_time = 30;

background_layer = layer_get_id("Background");
background_id = layer_background_get_id(background_layer);
layer_background_htiled(background_id, true);
layer_background_vtiled(background_id, true);