
if alarm[1] != -1{
	var cfog = gpu_get_fog();
	gpu_set_fog(true, c_white, 0, 0);
}

if hasShell{
	var vy = 0;
	if Input.down then vy = 3;
	draw_sprite_ext(spr_shell,cShell,x + drawShellXOffset*facing,y + drawShellYOffset + vy,facing,1,0,c_white,1);
}

if Input.down && hasShell {
	draw_sprite_ext(spr_player_hide,0,x,y+3,facing,1,0,c_white,1);
}else{
	draw_sprite_ext(spr_player,image_index,	x,y,facing,1,0,c_white,1);
}

if shellSmash && alarm[3]=-1{
	draw_sprite(spr_smash, 0, x, y);
}
if shellBounce{
	draw_sprite_ext(spr_smash, 0, x, y+16, 1, sign(-vsp), 0, c_white, 1);
}

gpu_set_fog(false, c_white, 0, 0);