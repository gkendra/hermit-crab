/*if hasShell{ //FOR DEBUGGING!
	cShell += mouse_wheel_up() - mouse_wheel_down();
	if cShell < 0 {cShell = 2}
	if cShell > 2 {cShell = 0}
}*/

if keyboard_check_pressed(vk_f12){
	if instance_exists(obj_door){
		with(obj_door){
			if alarm[0] =-1 then alarm[0] = changeTime;
			GUI_ScreenWipe(changeTime,c_black,1)
		}
	}
}


#region Movement
moveDir = 0
GroundCheck()
var water = place_meeting(x,y,obj_water);

#region INPUT!
if alarm[1] = -1 && !dead && !shellSmash && !frozen{ //If not paralyzed by enemy and not dead!
	
	var stuck = Input.down && cShell = 0 && hasShell;
	if Input.left && !stuck{
		moveDir = -1
		facing = -1
		moveBlock = noone;
	} else
	
	if Input.right && !stuck{
		moveDir = 1
		facing = 1
		moveBlock = noone;
	}
	
	if Input.jump && onGround {
		var jv = jumpVelocity;
		switch (cShell){
			case 1: jv *= 1.2; break;
			case 2: jv *= 0.8; break;
		}
		if !hasShell && water then jv *= 1.4;
		vsp -= jv;
		moveBlock = noone;
		playSound(snd_jump);
	}
	
	
	if Input.interact {
		//Drop shell
		if hasShell {
			dropShell();
		}
		else
		{ //Pick up shell
			if distance_to_object(obj_shell) < 20 {
				var shl = instance_nearest(x, y, obj_shell);
				pickupShell = shl;
				shl.jump = true;
				shl.vsp = -6;
				shl.y -= 3;
				shl.alarm[0]=2;
				alarm[0] = 0.5 * room_speed; //1 half second before timeout
				playSound(snd_jump);
			}
		}
	}
	
	if Input.down {
		switch(cShell){
			case 1: {
				if !shellBounce{
					shellBounce = true;
					if !place_meeting(x, y+1, par_solid){
						vsp = 10;
					}else{
						vsp = -5;
					}
				}
			break}
			case 2: {
				if place_meeting(x, y+1, par_solid){
					vsp = -jumpVelocity;
				}else{
					if !shellSmash{
						shellSmash = true;
						alarm[3] = 15;
					}
				}
			break}
		}
	}
}
#endregion


//Regular horizontal movement
if moveDir !=0{
	cSpeed += aSpeed;
	cSpeed = min(cSpeed, moveSpeed);
}else{
	if cSpeed >= 0.5{
		cSpeed -= 0.5;
	}else{
		cSpeed = 0;
	}
}
hsp = facing * cSpeed + khsp;


//Move blocks
handleMoveBlock();
hsp += mhsp;


//Animate
if moveDir != 0 {
	image_speed = 1;
}else if !frozen{
	image_speed = 0;
	image_index = 0;
}


//Knockback
if abs(khsp) > 0.5{
	khsp -= 0.4*sign(khsp);
}else{
	khsp = 0;
}

//Gravity
if onGround == false {
	var factor = 1;
	if water{
		factor = 0.25;
	}
	vsp += grav * factor;
	if vsp > 32 * factor then vsp = 32 * factor;
}

#region Shell SMASH!
if shellSmash = true{
	if alarm[3] != -1{ //Don't move while getting ready to SMASH
		vsp = 0;
		hsp = 0;
	}else{
		vsp = 10;
		if place_meeting(x,y+1,par_solid){
			shellSmash = false;
		}
	}
}
#endregion

if shellBounce{
	if !Input.down then shellBounce = false;
	if vsp > 0{
		if place_meeting(x,y+vsp+2,par_solid){
			vsp = -20;
			playSound(snd_jump);
		}
	}
}

var tvsp = vsp;
var factor = 1;
if water{
	vsp*=0.25;
}
HorizontalCollision()
VerticalCollision()
 
y += vsp
x += hsp
//hsp = 0
if vsp!=0{
	vsp = tvsp;
}


if y>room_height{
	playerDie();	
}
pushOutside();
#endregion


//Control camera
layer_x(background_layer, obj_camera.x*0.8);
layer_y(background_layer, obj_camera.y-obj_camera.gameheight/2);