{
    "id": "d7a65c6d-c207-4b27-af2c-1dafb48481bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "f2fe53b7-b7d3-48ee-81d6-8128d1ae64d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "edf1a95a-24e7-42ad-860c-2fb839ff881b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "da1a35f1-d502-4c36-ba99-c58de047aa99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "83f4d201-aaa2-4f2e-abc4-f736c450c26c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "cd6fdce5-3173-4d0a-ac87-c1516a5c040f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "392866a1-47cc-4d76-8c5f-9dccb6d97864",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        },
        {
            "id": "07dfc469-448b-4ccb-b572-3dc1a5d1e997",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "d7a65c6d-c207-4b27-af2c-1dafb48481bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "541ea1ec-3928-4b01-9c8c-614604e546be",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "hasShell",
            "varType": 3
        },
        {
            "id": "f5363925-8b98-4bb4-811a-87fe9e49acfd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 2,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "cShell",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "c4630b55-bbba-4b9a-8a7d-8cb0715b2b6f",
    "visible": true
}