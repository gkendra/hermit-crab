{
    "id": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy1",
    "eventList": [
        {
            "id": "4b90c0cb-0ce0-4ac0-b39d-c73c2061e772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2"
        },
        {
            "id": "d4e69329-2269-4116-adaa-17076409ee79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2"
        },
        {
            "id": "d771eb21-8960-475e-9104-85b59c4f013f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2"
        },
        {
            "id": "ddb0027c-cc26-4690-a326-fdbc84eba5c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d7a65c6d-c207-4b27-af2c-1dafb48481bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2"
        },
        {
            "id": "12fa7819-ed51-4501-be99-4be362bb3666",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a49c8eaf-a8ec-4ff7-9019-3980a7a895e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "66878562-0c8d-400a-9b25-b04ef9d32dfd",
    "visible": true
}