GroundCheck();

hsp = moveDir * moveSpeed + khsp;

if abs(khsp) > 0.5{
	khsp -= 0.4*sign(khsp);
}else{
	khsp = 0;
}

if !onGround {
	vsp += grav;
}

if HorizontalCollision() { //If collided
	moveDir = -moveDir;
	hsp = 0;
}
VerticalCollision()

if !place_meeting(x + 32*moveDir, y + 5, par_solid){
	moveDir = -moveDir;
	hsp = 0;
}

handleMoveBlock();
if mhsp != 0 then hsp = mhsp;

y += vsp
x += hsp
hsp = 0;

pushOutside();