 
//Test message
if place_meeting(x, y, obj_player) || place_meeting(x, y, obj_shell){
	if isPressed = false{
		isPressed = true;
		image_index = 1;
	
		if instance_exists(linkedObject) {
			linkedObject.buttonIsPressed = true	
			linkedObject.moving = true	
		}
		
		playSound(snd_unlock);
	}
}else{
	isPressed = false;
	image_index = 0;
	
	if instance_exists(linkedObject) {
		linkedObject.buttonIsPressed = false
		linkedObject.moving = false	
	}
	
}