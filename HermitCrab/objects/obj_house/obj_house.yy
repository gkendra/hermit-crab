{
    "id": "7a81f3a9-406e-4cba-90e9-290816c94f78",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_house",
    "eventList": [
        {
            "id": "18a7efde-69b5-4e90-ac7d-f371b5668f70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a81f3a9-406e-4cba-90e9-290816c94f78"
        },
        {
            "id": "2c84866b-1522-444a-8e55-7a84d5400881",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "7a81f3a9-406e-4cba-90e9-290816c94f78"
        },
        {
            "id": "a6137fe0-61fb-4cf3-b6d1-5696c6dbcf1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7a81f3a9-406e-4cba-90e9-290816c94f78"
        },
        {
            "id": "63a044f8-71e3-495b-a407-8365e24f4e75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7a81f3a9-406e-4cba-90e9-290816c94f78"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "572dadd0-b4a4-4bf6-9ba8-be776dc909fe",
    "visible": true
}