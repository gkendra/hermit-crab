
var press = false;
if (place_meeting(x, y, obj_player) && obj_player.cShell == 2) then press = true;
if (place_meeting(x, y, obj_shell) && instance_place(x,y,obj_shell).shellType == 2) then press = true;

if press{
	if isPressed = false{
		isPressed = true;
		image_index = 1;
	
		if instance_exists(linkedObject) {
			linkedObject.buttonIsPressed = true	
			linkedObject.moving = true	
		}
		playSound(snd_unlock);
	}
}else{
	isPressed = false;
	image_index = 0;
	
	if instance_exists(linkedObject) {
		linkedObject.buttonIsPressed = false
		linkedObject.moving = false	
	}
	
}