{
    "id": "7f577d83-81f6-4393-9e69-7c9aad19b128",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heavybutton",
    "eventList": [
        {
            "id": "192d860c-eef2-46de-b29d-5df14ee7ad70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f577d83-81f6-4393-9e69-7c9aad19b128"
        },
        {
            "id": "6104e5c6-652c-4635-868c-daad5b8f264b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f577d83-81f6-4393-9e69-7c9aad19b128"
        },
        {
            "id": "20e9af37-29d4-427a-a086-9af7361b1aef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7f577d83-81f6-4393-9e69-7c9aad19b128"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "7932a891-ac30-4ab9-a785-daebac2828d5",
    "visible": true
}