vsp = 0
hsp = 0

if !buttonIsPressed {
	if x != endX {
		hsp = moveSpeed * sign(endX - x)
	}
	
	if y != endY {
		vsp = moveSpeed * sign(endY - y)
	}
}

else if buttonIsPressed {
	if x != startX {
		hsp = moveSpeed * sign(startX - x)
	}
	
	if y != startY {
		vsp = moveSpeed * sign(startY - y)
	}
 } 


x += hsp 
y += vsp