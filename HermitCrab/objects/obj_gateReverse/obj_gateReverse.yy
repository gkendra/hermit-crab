{
    "id": "f481ade4-5e74-4438-9ead-fd2d6031aa4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gateReverse",
    "eventList": [
        {
            "id": "17361cf5-49a9-4409-9c82-b857a3efeccb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f481ade4-5e74-4438-9ead-fd2d6031aa4b"
        },
        {
            "id": "051f0cdf-69a4-4a1c-84c5-086fe810ce66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f481ade4-5e74-4438-9ead-fd2d6031aa4b"
        },
        {
            "id": "6ddf73da-2479-4387-bcec-d58475fe1932",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f481ade4-5e74-4438-9ead-fd2d6031aa4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "476be0bd-c201-442f-82cf-510cf06ca7f6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21eb21b0-f4aa-4dc1-b0a4-ecc4d158f9ef",
    "visible": true
}