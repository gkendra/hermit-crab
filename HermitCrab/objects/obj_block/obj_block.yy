{
    "id": "e865e509-4e16-4a7b-bb15-890116fedc97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block",
    "eventList": [
        {
            "id": "0353349a-5258-4851-9ff7-c49c2228be1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e865e509-4e16-4a7b-bb15-890116fedc97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "246e704b-a3ec-4137-871f-080a990b34ae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7287caeb-86e5-4b60-a449-98f146b6bd76",
    "visible": false
}