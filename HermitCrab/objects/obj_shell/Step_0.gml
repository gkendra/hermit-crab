GroundCheck();
var water = place_meeting(x,y,obj_water);
if jump then water = false;

if !onGround {
	var factor = 1;
	if water{
		factor = 0.25;
	}
	vsp += grav * factor;
	if vsp > 16 * factor then vsp = 16 * factor;
}

image_index = shellType;

if jump {
	moveBlock = noone;
	var tx = obj_player.x+obj_player.drawShellXOffset*obj_player.facing;
	var ty = obj_player.y+obj_player.drawShellYOffset;
	x = lerp(x, tx, 0.4);
	if obj_player.x != obj_player.xprevious{
		x = lerp(x, tx, 0.3);
	}
	
	if point_distance(x, y, tx, ty) < 5 && alarm[0]=-1 {
		event_user(0); //Get picked up by player
	}
}

var tvsp = vsp;
var factor = 1;
if water{
	vsp*=0.25;
}

if !jump{
	handleMoveBlock();
	hsp = mhsp;
	
	HorizontalCollision();
	VerticalCollision();
	x += hsp;
}

y += vsp;
vsp = tvsp;

if !jump then pushOutside();