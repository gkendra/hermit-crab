{
    "id": "a28e956e-3d3f-45ba-b03b-06e1fb4662d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shell",
    "eventList": [
        {
            "id": "e4969795-f50b-44b1-8d12-03141f60f13b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a28e956e-3d3f-45ba-b03b-06e1fb4662d9"
        },
        {
            "id": "ca760003-a75d-4ce9-92c4-6e972e133d38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a28e956e-3d3f-45ba-b03b-06e1fb4662d9"
        },
        {
            "id": "4c416c3b-6810-4cac-957f-b850ecca9455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a28e956e-3d3f-45ba-b03b-06e1fb4662d9"
        },
        {
            "id": "4da56569-5943-4205-aade-c8bdc8648ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a28e956e-3d3f-45ba-b03b-06e1fb4662d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "7055850a-a38a-44db-b277-d810b5f93658",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 2,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "shellType",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "9d052223-8b62-4e68-92d5-fe2e35a53507",
    "visible": true
}