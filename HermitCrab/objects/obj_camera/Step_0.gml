  //move x,y coordinate to xto, yto


if abs(xto -x) > 16 {
x += (xto -x)/24;
}

if abs(yto -y) > 16 {
y += (yto -y)/24;
}



if (follow != noone)
{
 xto = follow.x;
 yto = follow.y;

}


x = clamp(x, gamewidth/2, room_width - gamewidth/2);
y = clamp(y, gameheight/2, room_height - gameheight/2);

var vm = matrix_build_lookat(x,y,-1000,x,y,0,0,1,0);
camera_set_view_mat(camera,vm);

if Input.resize {
	var _full = window_get_fullscreen()
	window_set_fullscreen(!_full);
}