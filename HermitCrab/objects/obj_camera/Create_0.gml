   camera = camera_create();

mywidth = display_get_width();
myheight = display_get_height();
scalefactor = 3;

gamewidth = mywidth/scalefactor;
gameheight = myheight/scalefactor;

var vm = matrix_build_lookat(x,y,-1000,x,y,0,0,1,0);
var pm = matrix_build_projection_ortho(gamewidth,gameheight,1,10000);

camera_set_view_mat(camera,vm);
camera_set_proj_mat(camera,pm);

view_camera[0] = camera;

follow = noone
if instance_exists(obj_player) {
	follow = obj_player;
}
xto = x;  
yto = y;

if !view_enabled
   {
   view_enabled = true;
   }
   
  if !view_visible[0]
   {
   view_visible[0] = true;
   }

view_scale = 2;
window_width = gamewidth * view_scale;
window_height = gameheight * view_scale;
view_wport[0] = window_width;
view_hport[0] = window_height;
surface_resize(application_surface, gamewidth, gameheight);
window_set_size(window_width, window_height);
window_set_position(display_get_width()/2-window_width/2, display_get_height()/2-window_height/2);

if instance_exists(obj_player) {
	x = obj_player.x
	y = obj_player.y
}
/*display_set_gui_size(960,540) 