{
    "id": "4df7e995-b6fa-4ef6-a16b-85115c64e07e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_camera",
    "eventList": [
        {
            "id": "1c959c4a-0fd1-409f-ac71-cb7f944ff798",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4df7e995-b6fa-4ef6-a16b-85115c64e07e"
        },
        {
            "id": "5fb1d8fb-020f-4d8a-89e0-72f5a247017f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4df7e995-b6fa-4ef6-a16b-85115c64e07e"
        },
        {
            "id": "ac819b5b-b5c5-4e0a-8961-5275eb11bfb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4df7e995-b6fa-4ef6-a16b-85115c64e07e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}