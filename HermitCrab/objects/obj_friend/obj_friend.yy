{
    "id": "de954d56-c027-4320-8ece-8ba18c7e9fd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_friend",
    "eventList": [
        {
            "id": "37732a16-19dc-4adc-82c5-bc60e95c77f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de954d56-c027-4320-8ece-8ba18c7e9fd0"
        },
        {
            "id": "af3e27a5-ce3e-4b8d-9c69-6b650e8c5914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de954d56-c027-4320-8ece-8ba18c7e9fd0"
        },
        {
            "id": "92204906-92f9-49ac-b96a-119eacaad422",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "de954d56-c027-4320-8ece-8ba18c7e9fd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "5ea8d679-761a-4ea2-8589-a05f4fff1c5b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 5,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "image_index",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "b01d2d3a-c796-4daa-a2d2-9bd4a595f101",
    "visible": true
}