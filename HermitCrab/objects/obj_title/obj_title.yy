{
    "id": "e0a9cc03-0cb1-4a10-8e2e-5745fa40b427",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "56bcf375-94a4-4b06-b451-5bb87231b15d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0a9cc03-0cb1-4a10-8e2e-5745fa40b427"
        },
        {
            "id": "87fb7460-8ada-4a14-988d-4d2d9f75e367",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0a9cc03-0cb1-4a10-8e2e-5745fa40b427"
        },
        {
            "id": "52d95893-eff2-4c7e-b743-7e401e61207a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0a9cc03-0cb1-4a10-8e2e-5745fa40b427"
        },
        {
            "id": "822ea199-2d1e-46e4-a9c5-a70f41d1c807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e0a9cc03-0cb1-4a10-8e2e-5745fa40b427"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}