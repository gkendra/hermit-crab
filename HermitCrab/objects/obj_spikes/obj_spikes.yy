{
    "id": "cded536d-8579-422e-a6d6-03acbb608e4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spikes",
    "eventList": [
        {
            "id": "88ecb5f4-f68d-4f7a-9188-75a8ad18d3cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d7a65c6d-c207-4b27-af2c-1dafb48481bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cded536d-8579-422e-a6d6-03acbb608e4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f87efd0-6d06-49f9-abf3-86eb11fb2d34",
    "visible": true
}