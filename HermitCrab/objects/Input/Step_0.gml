if keyboard_check_pressed(vk_escape){
	if room = Level_Title{game_end()}else{room=Level_Title}
}
if keyboard_check_pressed(ord("R")){
	playerDie();
}

left = false
right = false
down = false
downpress = false
interact = false;
resize = false 

left =  keyboard_check(ord("A")) || keyboard_check(vk_left);
right =  keyboard_check(ord("D")) || keyboard_check(vk_right);
down = keyboard_check(ord("S")) || keyboard_check(vk_down);
jump = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up);
interact = keyboard_check_pressed(vk_space);

resize = keyboard_check_pressed(ord("P"))