{
    "id": "b7d0e912-d782-449b-9591-c7d990b91ec5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "cd875cb5-99ae-4142-aaf6-2873f67dbb23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b7d0e912-d782-449b-9591-c7d990b91ec5"
        },
        {
            "id": "cc474727-ff0a-40fe-a007-5265a4ec9766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b7d0e912-d782-449b-9591-c7d990b91ec5"
        },
        {
            "id": "a5bc8f83-1441-4581-9f7d-88eed1d7f3b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b7d0e912-d782-449b-9591-c7d990b91ec5"
        },
        {
            "id": "805dfe76-71fa-4e2f-9ed6-dbd02ed14c62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b7d0e912-d782-449b-9591-c7d990b91ec5"
        },
        {
            "id": "98b7f8b3-9d98-4617-91a2-051ee1c8e928",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b7d0e912-d782-449b-9591-c7d990b91ec5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "65183a03-be19-4884-b5a7-78b232cb4009",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "Level_tut1",
            "varName": "roomTo",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "1879eae1-7bb8-404c-a805-86798b1ad191",
    "visible": true
}