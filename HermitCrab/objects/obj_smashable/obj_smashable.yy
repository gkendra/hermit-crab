{
    "id": "002abd22-58c2-4e79-a2b5-4be0a8ce5b9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smashable",
    "eventList": [
        {
            "id": "1d38602a-c09d-4268-9f3b-ef315b5593a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "002abd22-58c2-4e79-a2b5-4be0a8ce5b9a"
        },
        {
            "id": "0c62bda2-ec3e-4365-960f-d4aaf445ed65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "002abd22-58c2-4e79-a2b5-4be0a8ce5b9a"
        }
    ],
    "maskSpriteId": "098247ee-cb92-4705-9ea8-6d5c0de07337",
    "overriddenProperties": null,
    "parentObjectId": "246e704b-a3ec-4137-871f-080a990b34ae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "098247ee-cb92-4705-9ea8-6d5c0de07337",
    "visible": true
}