{
    "id": "5608a2b6-e429-4bdc-bd75-0698b7110d07",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "m6x11",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c94e5f1b-dc01-488e-8d57-184d1ef76ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5dd0b37e-0dd1-48fc-a743-fcd239806da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 158,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "69b291be-9609-434f-bf32-8fa5e0bdccfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 149,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9cbf98d2-e2d0-4331-809d-05ac56e41c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 137,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a849e1bb-22d1-4ae1-b355-f33cd8509d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e05ef10a-811d-479d-8a50-2f386a5ae904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8033e233-eccf-48b4-9f57-9f0c215c9df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9cc086e3-86ed-428b-be71-18427c462c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bdc6a7ba-fdfd-4518-ad91-023d4d03fd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7974e219-fb94-4bb9-80fa-449f01da0ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 82,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cbfb47ab-30b5-4335-89a5-07d883141822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 163,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "70b376ac-a352-4649-8fce-f3b6595ee6ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2b633c37-1741-4e5d-be1a-4db193be9562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "41c467aa-206b-4896-9c6d-f3a3b7afe7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fbb92dde-2a9a-4418-9996-1762496d819e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 42,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "30a1a991-ecf1-42a4-a30d-9cac37e3a498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4c9d8133-1375-441d-aa29-dfc0b06253ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c8bf2d85-49b6-4685-b786-78741e62c97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1ad530a2-09d6-41a6-9f09-54100aa314b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "571315ed-48e5-4793-b740-77038a1851d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "90364ed0-3a2d-4842-97b4-e9ea1c9fbdfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 227,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "37f8e6d3-5bea-4dc3-9b99-66dc2936181e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b1a526ea-82d3-465d-b916-939f6491ec59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 172,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ef1d94d0-343a-4cf9-843a-ea12cf532777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 182,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fc8ecc84-1947-4000-9dc4-503f3ccbb94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "efecd0be-ba0d-4d2a-8cae-9fe93ffb0d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 144,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "20551f20-3e98-477b-b171-fdcade0c9b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 139,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b882ad71-bbf4-4305-98cd-882e55db4937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 134,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1d6f0651-5230-464d-a0fd-9ed12356fad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 125,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "08507d46-0056-4fb3-b571-6b47c72cc655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e1dfb01b-6415-4d74-9d18-833246c4a571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b8ad0f71-fa19-46ab-b966-dcd21ad0ff2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6e198692-52d6-4ee4-bef0-193868b7b573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 82,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8738b71d-bd8b-4792-b3f2-8673ff91dfa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "32a6e0a4-5de5-4546-b113-ac44cf8f842c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "888ae5ec-592b-486d-a8fc-0b90466e3d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e179f8c3-568c-44f8-aed0-466bab64586d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a0e74bc0-23e7-4f75-9867-7f76553d3624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d28d449e-9be7-4804-b62d-912dc2f55604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "eab2ce23-8818-44f9-95ef-9023648043b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "016bc236-7736-4d7e-a101-e3be56c08669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6c3df871-bfc3-4028-b7f3-e4ad6611107a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 245,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4267e86f-70a0-4ae3-b9a5-323e6b08d334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 235,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f7f48d8c-74a9-4c06-a804-6a73a9dd1383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ee4c3954-2034-4938-b04a-c612ad24d754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b1b7fb2f-1ec4-41a4-aada-e1badc0d5abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 202,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "da699cee-c944-49f7-977e-7f58211198ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 217,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "91f44709-b673-4087-9fb7-c2f27b74d131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 207,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1040270f-089f-4532-871e-ef65aace3201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 197,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5eea3465-90d4-4c91-a411-5edffde55314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ea8d5082-8f33-4887-a67b-cd78fd3a6fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "32f50b26-9d36-4786-a614-053893973ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "25c311cd-0563-4f21-8661-e06cb4fd0b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fff2cde7-5caa-4588-a367-8e2a2a99b288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1c2893e5-1113-4167-b6b2-45fe5744077b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5d8167aa-306b-49ae-a7e8-6114a99e4beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9675d1ed-6e10-4228-bd01-3d6e56781076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "65a9bf8c-7d7a-404a-bc63-ec38792723c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "55ed158b-b1a9-46da-a435-4ef51ae02b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bba9678a-95a3-4b9d-9fa9-ad01eda05a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ce860942-9dfe-4dc3-b6e7-432d9e5c4c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ec8bdeb0-2135-4542-ba6d-c858a35d4a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a521dcf3-631f-48c8-b042-80cd3bee9264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ae29d5a7-412b-407f-a0d6-8da04c91d6e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "de25185c-a4ee-4148-a3be-e99df263f77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "67d38ac8-a232-4c46-bb0f-ab5437e5c68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "308facb2-cbb8-4626-928d-7966ffd6f4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "92d8e547-5783-468f-a511-5ef5e2f6fb0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8d5f112c-2af9-4a97-b23b-72e0088a4284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e6618df8-bbb5-4527-aec1-087c5514d305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ad510857-d126-4588-89f5-627c0ffb0c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a0d21c57-938f-4ee4-bd04-abc483957bd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f1550b60-151d-4b04-b47b-9d09808241b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 81,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e67fb694-1117-481f-a838-95e65978e691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9939d6c6-41d9-41c2-95bc-9c365098ca42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "15d9d732-9477-4256-a9ad-2cd5798d4565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6f3734ae-8bc8-4181-b4fa-9c0edce16459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 164,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "44e05a6d-42b5-4c19-99ae-c6218b28a394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 151,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "96caecdc-16c6-4289-acac-2595f435d667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e5994e23-7d96-4a5d-ae03-3db565602fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6e97bfb2-4d59-4a51-8ed2-4c4ba5c05f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a3b2368f-91f4-4cfc-88e4-bad7257eea30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "579af6b0-adb5-4f85-9405-684c73abc94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "49e4c79a-3555-492e-95b9-d2ed9d490cab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 187,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "621807f4-f477-4eb5-b029-cfc8ef27ade9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 91,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a33c4b61-4f55-4c45-84a3-04cc2aed32b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 71,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "44059866-879c-4900-9aab-b9a0f78fae5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 61,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f6021342-bbf1-494f-8dd9-3155ad1ab091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 48,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a40038d3-4930-4fd8-b790-de56a848f667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "32622dea-8129-4a76-9253-f26394feaf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "91e99e4d-5cb8-4aed-b6bd-80f82f0d96f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 16,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8ce0450c-1a86-462c-8f5f-559d92ce2093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 7,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f6fc5dbb-19fc-4ea9-8f10-2b58337f2c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2a6e760a-df13-4322-88bd-5700cbda86ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ee53b45b-392f-465a-b577-2ffabbc2ab0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 154,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0b35fd28-982d-4e8d-8aa0-1509f3133902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 164,
                "y": 74
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 16,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}