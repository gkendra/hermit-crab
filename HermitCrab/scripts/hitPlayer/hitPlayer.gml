///hitPlayer([useDir])
///@arg [useDir]

var useDir = true;
if argument_count > 0{
	useDir = argument[0];
}

if obj_player.alarm[1]=-1{
	if useDir then obj_player.khsp = sign(obj_player.x - x)*8;
	with(obj_player){
		alarm[1] = paralyze_time;
		vsp = -10;
		if hasShell{
			dropShell();
		}else{
			playerDie();
		}
		playSound(snd_hit);
	}
}

