var _time = argument[0]
var _color = argument[1]
var _direction = argument[2]

if instance_exists(obj_GUI) {
	obj_GUI.screenWipe = true
	obj_GUI.wipeTime = _time
	obj_GUI.wipeColor = _color
	obj_GUI.target = 1;
	//obj_GUI.dir = _direction
	with obj_GUI {
		alarm_set(0,_time)
	}
}
