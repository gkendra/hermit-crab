var shl = instance_create_layer(x + drawShellXOffset*facing, y + drawShellYOffset, "Instances", obj_shell);
shl.shellType = cShell;
shl.image_index = cShell;
cShell = 0;
shl.image_xscale = facing;
shl.vsp = -6;
hasShell = false;
playSound(snd_dropshell);