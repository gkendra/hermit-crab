///Pushes self outside of solids. More of a last resort kinda thing.

if place_meeting(x,y,par_solid){
	var xdir = 0, ydir = 0;
	if !place_meeting(x + 32, y, par_solid){xdir = 1}else
	if !place_meeting(x - 32, y, par_solid){xdir = -1}else
	if !place_meeting(x, y + 32, par_solid){ydir = 1}else
	if !place_meeting(x, y - 32, par_solid){ydir = -1}
	x += xdir;
	y += ydir;
}