///@arg sound

var snd = audio_play_sound(argument0, 0, false);
var vol = 0.5;
if argument0 == snd_dropshell then vol = 2;
audio_sound_gain(snd, vol, 0);